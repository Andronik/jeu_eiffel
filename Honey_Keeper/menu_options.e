note
	description: "Classe qui g�re le menu 'OPTIONS'."
	author: "Andronik Karkaselis"
	date: "27-05-2020"

class
	MENU_OPTIONS

inherit
	MENU
		redefine
			make,
			run,
			update_scene,
			on_mouse_up
		end

create
	make

feature {NONE} -- Initialisation

	make (a_window: WINDOW; a_music: AUDIO; a_click: AUDIO; a_background: BACKGROUND)
			-- Envoie � son pr�curseur `a_window', `a_music', `a_click', `a_background' et cr�e les images pour afficher les buttons du menu
		do
			precursor (a_window, a_music, a_click, a_background)
			create music_button.make_displayable (a_window.window.renderer, "./Images/music_button.png")
			create effects_button.make_displayable (a_window.window.renderer, "./Images/effets_button.png")
			create muted_button.make_displayable (a_window.window.renderer, "./Images/muted.png")
			coordinates_list.extend (music_button_coordinates)
			coordinates_list.extend (effects_button_coordinates)
		end

feature {ANY}

	run
			-- D�marre les m�chaniques utilis�s dans le `menu_options'
		do
			from
				quit := False
				return_previous_menu := False
			until
				return_previous_menu or quit
			loop
				precursor {MENU}
			end
		end

	update_scene (a_timestamp: NATURAL_32)
			-- Redessine les �l�ments de la fen�tre et affecte les m�chaniques du jeu.
		do
			precursor (a_timestamp)
			launch_options_menu
			window.window.renderer.present
			window.window.update
		end

	on_mouse_up(a_timestamp: NATURAL_32; a_mouse_event: GAME_MOUSE_BUTTON_RELEASE_EVENT; a_nb_clicks: NATURAL_8)
			-- M�thode qui g�re le relachement d'un clic dans la fen�tre
		do
			precursor (a_timestamp, a_mouse_event, a_nb_clicks)
			validate_music_button
			validate_effects_button
			validate_return_button
		end

	validate_music_button
			-- Valide le bouton pour enlever la musique
		do
			if last_x > music_button_coordinates.x1 and last_x < music_button_coordinates.x2 and
			   last_y > music_button_coordinates.y1 and last_y < music_button_coordinates.y2
			then
				if music.is_muted then
					music.unmute
				else
					music.mute
				end
				click.play_once
			end
		end

	validate_effects_button
			-- Valide le bouton pour arr�ter les effets sonores
		do
			if last_x > effects_button_coordinates.x1 and last_x < effects_button_coordinates.x2 and
			   last_y > effects_button_coordinates.y1 and last_y < effects_button_coordinates.y2
			then
				if click.is_muted then
					click.unmute
				else
					click.mute
				end
				click.play_once
			end
		end

	launch_options_menu
			-- G�re l'affichage du `menu_options'
		do
			music_button.display (music_button_coordinates.x1, music_button_coordinates.y1, window.window.renderer)
			effects_button.display (effects_button_coordinates.x1, effects_button_coordinates.y1, window.window.renderer)
			return_button.display (return_button_coordinates.x1, return_button_coordinates.y1, window.window.renderer)
			if music.is_muted then
				muted_button.display (music_button_coordinates.x1, music_button_coordinates.y1, window.window.renderer)
			end
			if click.is_muted then
				muted_button.display (effects_button_coordinates.x1, effects_button_coordinates.y1, window.window.renderer)
			end
		end

feature -- Impl�mentation

	muted_button: DISPLAYABLE
			-- Effet pour d�montrer que la musique ou les effets sont arr�t�s

	music_button: DISPLAYABLE
			-- Bouton pour enlever la musique

	effects_button: DISPLAYABLE
			-- Bouton pour enlever les effets sonores

	music_button_coordinates: TUPLE [x1, y1, x2, y2:INTEGER]
			-- Liste de coordonn�es de `mute_button'
		once
			Result := [400, 180, 600, 235]
		end

	effects_button_coordinates: TUPLE [x1, y1, x2, y2:INTEGER]
			-- Liste de coordonn�es de `mute_button'
		once
			Result := [400, 280, 600, 335]
		end

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"
end
