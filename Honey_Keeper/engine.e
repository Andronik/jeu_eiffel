note
	description: "Classe qui g�re la m�chanique de l'application."
	author: "Andronik Karkaselis"
	date: "27-05-2020"
	
deferred class
	ENGINE

inherit

	GAME_LIBRARY_SHARED

	AUDIO_LIBRARY_SHARED

	IMG_LIBRARY_SHARED

feature {NONE} -- Initialisation

	make (a_window: WINDOW; a_music: AUDIO; a_click: AUDIO; a_background: BACKGROUND)
			-- Cr�e l'image de fond (background), les sons (music, click_sound), le curseur et une liste de coordonn�es pour les bouttons affich�s.
		do
			has_error := False
			window := a_window
			music := a_music
			click := a_click
			background := a_background
			create cursor.make
			create {LINKED_LIST [TUPLE [x1, y1, x2, y2: INTEGER]]} coordinates_list.make
			background.set_moving (True)
		ensure
			Window_assigned: window = a_window
			Music_assigned: music = a_music
			Click_assigned: click = a_click
			Background_assigned: background = a_background
		end

feature {ANY} -- Acc�s

	run
			-- D�marre l'affichage les m�chaniques utilis�s dans l'application.
		require
			No_Error: not has_error
		do
			game_library.clear_all_events
			game_library.quit_signal_actions.extend (agent on_quit)
			window.window.mouse_button_pressed_actions.extend (agent on_mouse_down)
			window.window.mouse_button_released_actions.extend (agent on_mouse_up)
			window.window.mouse_motion_actions.extend (agent on_mouse_move)
			game_library.iteration_actions.extend (agent update_scene)
			game_library.launch
		end

	update_scene (a_timestamp: NATURAL_32)
			-- Redessine les �l�ments de la fen�tre.
		do
			window.window.renderer.clear
			background.moving_background (last_x)
			background.draw (window)
			audio_library.update
--			io.put_string ("(" + last_x.out + "," + last_y.out + ")%N")
		end

feature {NONE}

	on_mouse_down(a_timestamp: NATURAL_32; a_mouse_event: GAME_MOUSE_BUTTON_PRESS_EVENT; a_nb_clicks: NATURAL_8)
			-- M�thode qui g�re l'enfoncement d'un clic dans la fen�tre
		do
			cursor.change_cursor
		end

	on_mouse_up(a_timestamp: NATURAL_32; a_mouse_event: GAME_MOUSE_BUTTON_RELEASE_EVENT; a_nb_clicks: NATURAL_8)
			-- M�thode qui g�re le relachement d'un clic dans la fen�tre
		do
			cursor.change_cursor
		end

	on_mouse_move(a_timestamp: NATURAL_32; a_mouse_event: GAME_MOUSE_MOTION_EVENT; a_delta_x, a_delta_y: INTEGER_32)
			-- M�thode qui met � jour les coordonn�es de la souris � chaque mouvement (`a_mouse_event')
		do
			last_x := a_mouse_event.x
			last_y := a_mouse_event.y
		end

	on_quit(a_timestamp:NATURAL_32)
			-- M�thode qui ferme l'application
		do
			quit := True
			game_library.stop
		end

feature {ANY}

	has_error: BOOLEAN
			-- `True' si un erreur est survenu durant la cr�ation de `Current'

	quit: BOOLEAN
			-- Indique si on quitte le jeu

	return_previous_menu: BOOLEAN
			-- Indique si on retourne au menu pr�c�dent

	window: WINDOW
			-- Fen�tre principale de l'application

	last_x, last_y: INTEGER
			-- Coordonn�es de la souris

	cursor: PLAYER_CURSOR
			-- Le curseur

	background: BACKGROUND
			-- L'image de fond

	music: AUDIO
			-- La musique de l'application

	click: AUDIO
			-- L'effet sonore lors d'un clic

	coordinates_list: LIST [TUPLE [x1, y1, x2, y2: INTEGER]]
			-- Liste de coordonn�es des bouttons du menu

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"
end
