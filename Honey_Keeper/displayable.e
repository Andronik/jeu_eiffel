note
	description: "Classe qui cr�e toutes les images du jeux."
	author: "Andronik Karkaselis"
	date: "27-05-2020"

class
	DISPLAYABLE

inherit
	IMAGES

create
	make_displayable

feature {NONE} -- Initialisation

	make_displayable (a_renderer: GAME_RENDERER; a_filename: STRING)
			-- M�thode qui lance la cr�ation d'un fichier (a_nom_fichier) dans la fen�tre (a_renderer).
		require
			No_Error: not has_error
			valid_file_type: a_filename.ends_with (".png")
			valid_filname_size: a_filename.count > 4
		do
			make_image (a_renderer, a_filename)
		end

feature {ANY} -- Acc�s

	display (a_pos_x, a_pos_y: INTEGER; a_renderer: GAME_RENDERER)
			-- Afficher l'image aux positions a_pos_x et a_pos_y dans la fen�tre (a_renderer).
		do
			a_renderer.draw_texture (Current, a_pos_x, a_pos_y)
		end

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"
end
