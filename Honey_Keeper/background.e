note
	description: "Classe qui cr�e et affiche l'image de fond."
	author: "Andronik Karkaselis"
	date: "27-05-2020"

class
	BACKGROUND

create
	make

feature {NONE} -- Initialisation

	make (a_window: WINDOW)
			-- Cr�e l'image de fond dans la fen�tre (a_window).
		do
			create image.make_displayable (a_window.window.renderer, "./Images/background.png")
			one_background := False
		end

feature {ANY} -- Acc�s

	draw (a_window: WINDOW)
			-- Affiche l'image de fond dans la fen�tre (a_window).
		local
			l_tile_x, l_tile_y:INTEGER
		do
			l_tile_x := 0
			l_tile_y := 0
			if one_background then
				image.display (l_tile_x, l_tile_y, a_window.window.renderer)
			else
				image.display (tile_x - 500, l_tile_y, a_window.window.renderer)
				image.display (tile_x + 500, l_tile_y, a_window.window.renderer)
			end
		end

	moving_background (a_value: INTEGER)
			-- Calcule la position `tile_x' de l'image de fond selon `a_value' qui est la position de la souris
		do
			if is_moving then
				if a_value <= 500 and tile_x < 500 then
					tile_x := tile_x + 1
				elseif a_value > 500 and tile_x > -500 then
					tile_x := tile_x - 1
				end
				if tile_x = 500 or tile_x = -500 then
					tile_x := 100
				end
			else
				if tile_x < 100 or tile_x > 100 then
					tile_x := tile_x + 10
				else
					one_background := True
				end
			end
		end

	set_moving (a_moving_bool: BOOLEAN)
			-- Change `is_moving' selon le boolean envoy�
		do
			is_moving := a_moving_bool
		end

feature {ANY} -- Acces

	is_moving: BOOLEAN
			-- Determine si l'image de fond bouge.

	one_background: BOOLEAN
			-- Determine si seulement une image de fond est utilis�e.

	image: DISPLAYABLE
			-- L'image des tuiles hexagonales.

	tile_x: INTEGER
			-- Position de l'image sur l'axe 'x'.

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "[C�gep de Drummondville]"
end
