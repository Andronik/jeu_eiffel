note
	description: "Classe qui contient les m�thodes de gestion du score du joueur."
	author: "Andronik Karkaselis"
	date: "27-05-2020"

deferred class
	TEXT_OVERLAY

feature

	make_overlay (a_window: GAME_RENDERER; a_font_size: INTEGER)
			-- Cr�� le texte qui affiche le score et le combo du joueur
		do
			window := a_window
			formatted_score := "000000"
			create color_in.make_rgb (0, 0, 0)
			create color_out.make_rgb (255, 248, 98)
			create font_in.make ("./Fonts/CollegiateInsideFLF.ttf", a_font_size)
			if font_in.is_openable then
				font_in.open
			else
				has_error := True
			end
			create font_out.make ("./Fonts/CollegiateOutlineFLF.ttf", a_font_size)
			if font_out.is_openable then
				font_out.open
			else
				has_error := True
			end
			create my_score_texture_in.make_from_surface (a_window, create {TEXT_SURFACE_BLENDED}.make ("SCORE:", font_in, color_in))
			create my_score_texture_out.make_from_surface (a_window, create {TEXT_SURFACE_BLENDED}.make ("SCORE:", font_out, color_out))
			create score_texture_in.make_from_surface (window, create {TEXT_SURFACE_BLENDED}.make (formatted_score, font_in, color_in))
			create score_texture_out.make_from_surface (window, create {TEXT_SURFACE_BLENDED}.make (formatted_score, font_out, color_out))
		end

feature -- Impl�mentation

	has_error: BOOLEAN
			-- Indique la pr�sence d'erreur

	color_in, color_out: GAME_COLOR
			-- Couleur utilis�e pour l'affichage de texte

	formatted_score: STRING
			-- Contient le score

	score_texture_in, score_texture_out: GAME_TEXTURE
			-- Texture du score

	my_score_texture_in, my_score_texture_out: GAME_TEXTURE
			-- Texture de 'SCORE: '

	font_in, font_out: TEXT_FONT
			-- Police utilis�e

	window: GAME_RENDERER
			-- Fen�tre de l'application

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "[C�gep de Drummondville]"
end
