note
	description: "Classe qui contient les m�thodes de gestion du menu aide."
	author: "Andronik Karkaselis"
	date: "27-05-2020"

class
	MENU_AIDE

inherit
	MENU
		redefine
			make,
			run,
			update_scene,
			on_mouse_up
		end

create
	make

feature {NONE}

	make (a_window: WINDOW; a_music: AUDIO; a_click: AUDIO; a_background: BACKGROUND)
			-- Envoie � son pr�curseur `a_window', `a_music', `a_click', `a_background' et cr�e les images pour afficher les instructions
		do
			precursor (a_window, a_music, a_click, a_background)
			create instructions.make_displayable (a_window.window.renderer, "./Images/instructions.png")
			create overlay.make_displayable (a_window.window.renderer, "./Images/game_over_lay.png")
		end

feature {ANY}

	run
			-- D�marre les m�chaniques utilis�s dans le `menu_aide'
		do
			from
				quit := False
				return_previous_menu := False
			until
				return_previous_menu or quit
			loop
				precursor {MENU}
			end
		end

	update_scene (a_timestamp: NATURAL_32)
			-- Redessine les �l�ments de la fen�tre et affecte les m�chaniques du jeu.
		do
			precursor (0)
			launch_aide_menu
			window.window.renderer.present
			window.window.update
		end

	on_mouse_up(a_timestamp: NATURAL_32; a_mouse_event: GAME_MOUSE_BUTTON_RELEASE_EVENT; a_nb_clicks: NATURAL_8)
			-- M�thode qui g�re le relachement d'un clic dans la fen�tre
		do
			precursor (a_timestamp, a_mouse_event, a_nb_clicks)
			validate_return_button
		end

	launch_aide_menu
			-- Affiche les �l�ments du `menu_aide'				
		do
			overlay.display (90, 40, window.window.renderer)
			instructions.display (100, 80, window.window.renderer)
			return_button.display (return_button_coordinates.x1, return_button_coordinates.y1, window.window.renderer)
		end

feature -- Impl�mentation

	instructions: DISPLAYABLE
			-- Image du texte

	overlay: DISPLAYABLE
			-- Image du fond pour les instructions

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"
end
