note
	description: "Classe qui contient les m�thodes li�es � la musique et les effets sonores."
	author: "Andronik Karkaselis"
	date: "27-05-2020"

class
	AUDIO

inherit

	AUDIO_LIBRARY_SHARED

create
	make

feature {NONE} -- Initialisation

	make (a_sound_file:STRING)
			-- Cr�e un `sound' avec le nom du fichier pass� en argument.
		require
			Valid_file_name: a_sound_file.count > 4
			Valid_audio_type: a_sound_file.ends_with (".wav")
		do
			has_error := False
			audio_library.sources_add
			source:=audio_library.last_source_added
			create sound.make(a_sound_file)
			if sound.is_openable then
				sound.open
			else
				has_error := True
--				io.put_string ("Impossible d'ouvrir le fichier " + a_sound_file + ".%N")
			end
		end

feature -- Attributs

	has_error: BOOLEAN
			-- True if there's an error.
	sound: AUDIO_SOUND_FILE
			-- The sound to be played.
	source: AUDIO_SOURCE
			-- The source where the sound is.

feature -- M�thodes

	play_once
			-- Joue le `sound' une seule fois.
		require
			No_Error: not has_error
		do
			if sound.is_open then
				source.queue_sound (sound)
				source.play
			end
		end

	play_loop
			-- Joue le ficher audio en continue.
		require
			No_Error: not has_error
		do
			if sound.is_open then
				source.queue_sound_infinite_loop (sound)
				source.play
			end
		end

	mute
			-- Change le volume de `source' � 0
		do
			source.set_gain (0)
--			is_muted :=  True
		ensure
			Valid_muting: is_muted
		end

	unmute
			-- Retourne le son � la normale
		do
			source.set_gain (1)
--			is_muted := False
		ensure
			Valid_unmuting: not is_muted
		end

	is_muted: BOOLEAN
			-- Contient `True' si le son est muet.
		do
			Result := source.gain = 0
		end

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "[C�gep de Drummondville]"
end
