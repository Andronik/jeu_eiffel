note
	description: "Classe qui contient les m�thodes de gestion du menu options."
	author: "Andronik Karkaselis"
	date: "27-05-2020"

class
	MENU_GAMEOVER

inherit

	MENU
		redefine
			make,
			run,
			update_scene,
			on_mouse_up
		end

	TEXT_OVERLAY
		rename
			has_error as text_has_error,
			window as renderer
		end

create
	make

feature {NONE} -- Initialisation

	make (a_window: WINDOW; a_music: AUDIO; a_click: AUDIO; a_background: BACKGROUND)
			-- Envoie � son pr�curseur `a_window', `a_music', `a_click', `a_background' et cr�e les �l�ments du menu 'gameover'
		do
			precursor (a_window, a_music, a_click, a_background)
			create game_over_lay.make_displayable (a_window.window.renderer, "./Images/game_over_lay.png")
			make_overlay (a_window.window.renderer, 40)
			create gameover_texture_in.make_from_surface (renderer, create {TEXT_SURFACE_BLENDED}.make ("GAME OVER", font_in, color_in))
			create gameover_texture_out.make_from_surface (renderer, create {TEXT_SURFACE_BLENDED}.make ("GAME OVER", font_out, color_out))
			create new_record_texture_in.make_from_surface (renderer, create {TEXT_SURFACE_BLENDED}.make ("NOUVEAU RECORD!", font_in, color_in))
			create new_record_texture_out.make_from_surface (renderer, create {TEXT_SURFACE_BLENDED}.make ("NOUVEAU RECORD!", font_out, color_out))
			create name_texture_in.make_from_surface (renderer, create {TEXT_SURFACE_BLENDED}.make ("NOM: ", font_in, color_in))
			create name_texture_out.make_from_surface (renderer, create {TEXT_SURFACE_BLENDED}.make ("NOM: ", font_out, color_out))
			create name.make
			create entered_name_texture_in.make_from_surface (renderer, create {TEXT_SURFACE_BLENDED}.make ("Entrer un nom", font_in, color_in))
			create entered_name_texture_out.make_from_surface (renderer, create {TEXT_SURFACE_BLENDED}.make ("Entrer un nom", font_out, color_out))
			final_score := " "
			player_name := ""
			is_new_highscore := False
			format_final_score
			create net.make
		end

feature {ANY} -- Acc�s

	run
			-- D�marre l'affichage les m�chaniques utilis�s dans le `menu_gameover'
		do
			format_final_score
			from
				quit := False
				return_previous_menu := False
			until
				quit or return_previous_menu
			loop
				game_library.clear_all_events
				game_library.quit_signal_actions.extend (agent on_quit)
				window.window.mouse_button_pressed_actions.extend (agent on_mouse_down)
				window.window.mouse_button_released_actions.extend (agent on_mouse_up)
				window.window.mouse_motion_actions.extend (agent on_mouse_move)
				window.window.start_text_input
				window.window.key_pressed_actions.extend (agent on_key_pressed)
				window.window.text_input_actions.extend (agent on_text_input)
				game_library.iteration_actions.extend (agent update_scene)
				game_library.launch
				name.wipe_out
			end
			window.window.stop_text_input
			game_library.clear_all_events
		end

	update_scene (a_timestamp: NATURAL_32)
			-- Redessine les �l�ments de la fen�tre du menu 'gameover'
		do
			precursor (0)
			launch_gameover_overlay
			display_name
			display_final_score
			window.window.renderer.present
			window.window.update
		end

	on_mouse_up (a_timestamp: NATURAL_32; a_mouse_event: GAME_MOUSE_BUTTON_RELEASE_EVENT; a_nb_clicks: NATURAL_8)
			-- M�thode qui g�re le relachement d'un clic dans la fen�tre
		do
			precursor (a_timestamp, a_mouse_event, a_nb_clicks)
			validate_return_button
		end

	on_text_input (a_timestamp: NATURAL_32; a_character: STRING_32)
			-- Ajoute `a_character' dans `name'
		do
			if not name.is_empty then
				if name.last.character.count < 12 then
					name.last.character := name.last.character + a_character
					name.extend (name.last.character)
				end
			else
				name.extend (a_character)
			end
			player_name := player_name + a_character
			update_name
		end

	on_key_pressed (a_timestamp: NATURAL_32; a_key_state: GAME_KEY_STATE)
			-- Enl�ve la derni�re lettre entr�e dans `name'
		do
			if a_key_state.is_backspace and not name.last.character.is_empty then
				name.last.character := name.last.character.substring (1, name.last.character.count - 1)
				player_name.remove_tail (1)
			end
			update_name
		end

	update_name
			-- Recr�e la texture du nom que le joueur entre � l'�cran
		do
			across
				name as la_name
			loop
				create entered_name_texture_in.make_from_surface (window.window.renderer, create {TEXT_SURFACE_BLENDED}.make (la_name.item.character, font_in, color_in))
				create entered_name_texture_out.make_from_surface (window.window.renderer, create {TEXT_SURFACE_BLENDED}.make (la_name.item.character, font_out, color_out))
			end
		end

	launch_gameover_overlay
			-- Redessine les �l�ments de l'affichage 'gameover'
		do
			game_over_lay.display (90, 40, window.window.renderer)
			renderer.draw_texture (gameover_texture_in, 370, 100)
			renderer.draw_texture (gameover_texture_out, 370, 100)
			return_button.display (return_button_coordinates.x1, return_button_coordinates.y1, window.window.renderer)
		end

	display_name
			-- Redessine les lettres du nom entr�e � l'�cran
		do
			renderer.draw_texture (entered_name_texture_in, 410, 320)
			renderer.draw_texture (entered_name_texture_out, 410, 320)
		end

	display_final_score
			-- Redessine le score du joueur quand il perd
		do
			renderer.draw_texture (my_score_texture_in, 230, 180)
			renderer.draw_texture (my_score_texture_out, 230, 180)
			renderer.draw_texture (score_texture_in, 430, 180)
			renderer.draw_texture (score_texture_out, 430, 180)
			renderer.draw_texture (name_texture_in, 230, 320)
			renderer.draw_texture (name_texture_out, 230, 320)
			if is_new_highscore then
				renderer.draw_texture (new_record_texture_in, 300, 250)
				renderer.draw_texture (new_record_texture_out, 300, 250)
			end
		end

	set_final_score (a_final_score: STRING_8; a_highscore: STRING_8)
			-- Assigne le score final du joueur
		do
			final_score := a_final_score
			current_highscore := a_highscore.to_integer
		ensure
			Valid_score: final_score = a_final_score
		end

	check_highscore
		do
			if current_highscore < final_score.to_integer then
				is_new_highscore := True
			else
				is_new_highscore := False
			end
		end

	send_highscore
			-- Envoi le score record du jeu
		do
			if is_new_highscore then
				net.client (player_name + "|" + final_score.out)
			end
		end

	format_final_score
			-- Cr�� les textures du score final
		do
			create score_texture_in.make_from_surface (renderer, create {TEXT_SURFACE_BLENDED}.make (final_score, font_in, color_in))
			create score_texture_out.make_from_surface (renderer, create {TEXT_SURFACE_BLENDED}.make (final_score, font_out, color_out))
		end

feature {ANY} -- Implementation

	net: NETWORK
			-- Permet l'utilisation de la classe `NETWORK'

	is_new_highscore: BOOLEAN
			-- Indique si le nouveau score est un nouveau score record

	gameover_texture_in, gameover_texture_out: GAME_TEXTURE
			-- Texture de 'GAME OVER'

	new_record_texture_in, new_record_texture_out: GAME_TEXTURE
			-- Texture de 'NOUVEAU RECORD'

	name_texture_in, name_texture_out: GAME_TEXTURE
			-- Texture de 'NOM:'

	entered_name_texture_in, entered_name_texture_out: GAME_TEXTURE
			-- Texture du nom entr�e par le joueur.

	final_score: STRING
			-- Contient le score final en string pour faciliter l'affichage

	current_highscore: INTEGER
			-- Contient le highscore

	game_over_lay: DISPLAYABLE
			-- Image affich� au fond

	name: LINKED_LIST [TUPLE [character: STRING_32]]
			-- Liste contenant les lettres du nom entr�e

	player_name: STRING
			-- Le nom entr� par le joueur en STRING

invariant

note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"

end
