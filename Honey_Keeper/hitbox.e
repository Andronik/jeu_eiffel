note
	description:  "Classe qui g�re les boites de collisions."
	author: "Andronik Karkaselis"
	date: "27-05-2020"

class
	HITBOX

create
	make

feature {NONE} -- Initialisation

	make(a_x1, a_y1, a_x2, a_y2:INTEGER)
			-- Initialise les positions envoy�s � celles de la classe
		require
			Valid_x1: a_x1 >= 0
			Valid_y1: a_y1 >= 0
			Valid_x1: a_x2 >= 0
			Valid_y1: a_y2 >= 0
		do
			x1 := a_x1
			y1 := a_y1
			x2 := a_x2
			y2 := a_y2
		end

feature -- Access

	is_inside(a_x, a_y:INTEGER):BOOLEAN
			-- V�rifie si le click se trouve entre les limites d'une boite de collision
		do
			Result := a_x > x1 and a_x < x2 and a_y > y1 and a_y < y2
		end

	x1:INTEGER
			-- Position gauche sur l'axe 'x'

	y1:INTEGER
			-- Position haute sur l'axe 'y'

	x2:INTEGER
			-- Position droite sur l'axe 'x'

	y2:INTEGER
			-- Position basse sur l'axe 'x'

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"
end
