note
	description:  "Classe qui g�re la m�chanique du score record affich�."
	author: "Andronik Karkaselis"
	date: "27-05-2020"

class
	HIGHSCORE

inherit
	TEXT_OVERLAY

create
	make

feature {NONE} -- Initialisation

	make (a_window: GAME_RENDERER)
			-- Construit le texte qui affich� le `highscore'
		require
			No_error: not has_error
		do
			make_overlay (a_window, 24)
			highscore := "000000"
			highscore_name := " "
			update_highscore
		end

feature {ANY} -- Access

	display_highscore
			-- Afficher le `highscore' (ses textures)
		do
			window.draw_texture (highscore_texture_in, 05, 06)
			window.draw_texture (highscore_texture_out, 05, 06)
			window.draw_texture (highscore_number_texture_in, 24, 30)
			window.draw_texture (highscore_number_texture_out, 24, 30)
			window.draw_texture (highscore_name_texture_in, 5, 54)
			window.draw_texture (highscore_name_texture_out, 5, 54)
		end

	update_highscore
			-- Met � jour les textures pour afficher `highscore'
		do
			create highscore_texture_in.make_from_surface (window, create {TEXT_SURFACE_BLENDED}.make ("HIGHSCORE:", font_in, color_in))
			create highscore_texture_out.make_from_surface (window, create {TEXT_SURFACE_BLENDED}.make ("HIGHSCORE:", font_out, color_out))
			create highscore_number_texture_in.make_from_surface (window, create {TEXT_SURFACE_BLENDED}.make (highscore, font_in, color_in))
			create highscore_number_texture_out.make_from_surface (window, create {TEXT_SURFACE_BLENDED}.make (highscore, font_out, color_out))
			create highscore_name_texture_in.make_from_surface (window, create {TEXT_SURFACE_BLENDED}.make (highscore_name, font_in, color_in))
			create highscore_name_texture_out.make_from_surface (window, create {TEXT_SURFACE_BLENDED}.make (highscore_name, font_out, color_out))
		end

	set_highscore (a_highscore: STRING; a_highscore_name: STRING)
			-- Met � jour les variable `highscore' et `highscore_name'
		do
			highscore := a_highscore
			highscore_name := a_highscore_name
			update_highscore
		end

feature {ANY}

	highscore: STRING
			-- Contient le score

	highscore_name: STRING
			-- Contient le nom

	highscore_texture_in, highscore_texture_out: GAME_TEXTURE
			-- texture pour afficher le mot 'HIGHSCORE'

	highscore_number_texture_in, highscore_number_texture_out: GAME_TEXTURE
			-- texture pour afficher le `highscore'

	highscore_name_texture_in, highscore_name_texture_out: GAME_TEXTURE
			-- texture pour afficher le `highscore_name'

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"
end
