note
	description:  "Classe qui g�re la connection r�seau."
	author: "Andronik Karkaselis"
	date: "27-05-2020"


class
	NETWORK

inherit
	GAME_LIBRARY_SHARED

create
	make

feature {NONE} -- Initialization

	make
			-- Construit le socket pour communiquer avec le serveur
		local
			l_port: INTEGER
			l_host: STRING
		do
			l_port := 3636
			l_host := set_host
			information := ""
			create socket.make_targeted (l_host, l_port)
			create {LINKED_LIST [STRING]} highscore_list.make
		end

feature {ANY} -- Acces

	client (a_message: STRING)
			-- Envoie les informations au serveur
		do
			socket.put_integer (a_message.count)
			socket.put_string (a_message)
			socket.close
		end

	client_listener
			-- Recoit l'information du serveur
		local
			l_socket: NETWORK_DATAGRAM_SOCKET
			l_thread: NET_THREAD
		do
			create l_socket.make_bound (3036)
			create l_thread.make (l_socket)
			l_thread.launch
			from
			until
				l_thread.is_over
			loop
				game_library.delay (10)
			end
			l_socket.close
			l_thread.join
			highscore_list := l_thread.highscore_list
		end

	set_host: STRING
			-- Retourne le host auquel il faut se connecter
			-- Techniquement, on doit pas recompiler pour changer de host...
		local
			l_text: STRING_8
			l_file: PLAIN_TEXT_FILE
		do
			l_text := "localhost"
			create l_file.make_open_read ("host.txt")
			from
			until
				l_file.exhausted
			loop
				l_file.read_line
				if not l_file.last_string.is_empty then
					l_text := l_file.last_string
				end
			end
			l_file.close
			Result := l_text
		end


feature {ANY} -- Implementation

	socket: NETWORK_DATAGRAM_SOCKET
			-- Le socket utilis� pour communiquer

	information: STRING
			-- L'information envoy�e au serveur

	highscore_list: LIST [STRING]
			-- Liste contenant le nom et le score record.


invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"

end
