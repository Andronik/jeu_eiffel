note
	description: "Classe qui contient les m�thodes de gestion d'un menu."
	author: "Andronik Karkaselis"
	date: "5-5-2020"

deferred class -- Classe sous construction en ce moment
	MENU

inherit

	ENGINE
		redefine
			make
		end

feature -- Initialisation

	make (a_window: WINDOW; a_music: AUDIO; a_click: AUDIO; a_background: BACKGROUND)
			-- Envoie � son pr�curseur `a_window', `a_music', `a_click', `a_background' et cr�e le bouton `return_button'
		do
			precursor (a_window, a_music, a_click, a_background)
			create return_button.make_displayable (a_window.window.renderer, "./Images/return_button.png")
			coordinates_list.extend (return_button_coordinates)
		end

feature {ANY} -- Acc�s

	validate_return_button
			-- Valide que le bouton retour � bien �t� click� et commence le retour au menu pr�c�dent
		do
			if last_x > return_button_coordinates.x1 and last_x < return_button_coordinates.x2 and
			   last_y > return_button_coordinates.y1 and last_y < return_button_coordinates.y2
			then
				click.play_once
				return_previous_menu := True
				quit := False
				game_library.stop
			end
		end

	return_button: DISPLAYABLE
			-- L'image du bouton de retour `return_button'

	return_button_coordinates: TUPLE [x1, y1, x2, y2: INTEGER]
			-- Coordonn�s du bouton de retour `return_button'
		once
			Result := [730, 500, 930, 555]
		end

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"
end
