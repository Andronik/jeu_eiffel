note
	description: "Classe qui contient les m�thodes de gestion du score du joueur."
	author: "Andronik Karkaselis"
	date: "27-05-2020"

class
	SCORE

inherit
	TEXT_OVERLAY

create
	make

feature

	make (a_window: GAME_RENDERER)
			-- Cr�� le texte qui affiche le score et le combo du joueur
		do
			make_overlay (a_window, 40)
			display_score
			create combo_font_in.make ("./Fonts/CollegiateInsideFLF.ttf", 64)
			combo_font_in.open
			create combo_font_out.make ("./Fonts/CollegiateOutlineFLF.ttf", 64)
			combo_font_out.open
			reset_combo_meter
			display_combo
		end
feature

	display_score
			-- Affiche le score du joueur
		require
			No_Error: not has_error
		do
			create score_texture_in.make_from_surface (window, create {TEXT_SURFACE_BLENDED}.make (formatted_score, font_in, color_in))
			create score_texture_out.make_from_surface (window, create {TEXT_SURFACE_BLENDED}.make (formatted_score, font_out, color_out))
			window.draw_texture (score_texture_in, 805, 42)
			window.draw_texture (score_texture_out, 805, 42)
			window.draw_texture (my_score_texture_in, 807, 06)
			window.draw_texture (my_score_texture_out, 807, 06)
		end

	update_score (a_added_score: INTEGER)
			-- Actualise le score du joueur dans le bon format pour �tre affich� (`formatted_score')
		local
			l_format: FORMAT_INTEGER
		do
			create l_format.make (6)
			l_format.set_fill ('0')
			current_score := current_score + a_added_score
			formatted_score := l_format.formatted (current_score)
		end

	display_combo
			-- Affiche le combo du joueur
		do
			create combo_text_in.make (combo_meter, combo_font_in, color_in)
			create combo_text_out.make (combo_meter, combo_font_out, color_out)
			create combo_texture_in.make_from_surface (window, combo_text_in)
			create combo_texture_out.make_from_surface (window, combo_text_out)
			window.draw_texture (combo_texture_in, 820, 82)
			window.draw_texture (combo_texture_out, 820, 82)
		end

	update_combo
			-- Actualise le combo du joueur
		do
			combo_index := combo_index + 1
			if combo_index = 7 then
				is_bonus := True
			else
				is_bonus := False
			end
			if combo_index > 7 then
				combo_index := 2
			end
		end

	reset_combo_meter
			-- R�initialise le combo du joueur
		do
			combo_index := 1
			is_bonus := False
		end

feature -- Implementation

	is_bonus: BOOLEAN
			-- L'information de si c'est un bonus ou non

	combo_index: INTEGER
			-- Le num�ro d'index du combo

	current_score: INTEGER
			-- Le score du joueur

	combo_meter: STRING
			-- Le text qui indique le combo
		do
			Result := combo_meter_values.at(combo_index)
		end

	combo_meter_values: CHAIN[STRING]
			-- Les valeurs affich�s du combo
		once
			create {ARRAYED_LIST[STRING]} Result.make (7)
			Result.extend (" ")
			Result.extend ("x 1")
			Result.extend ("x 2")
			Result.extend ("x 3")
			Result.extend ("x 4")
			Result.extend ("x 5")
			Result.extend ("100!")
		end

	combo_font_in, combo_font_out: TEXT_FONT
			-- Police utilis� pour le `combo_meter'

	combo_text_in, combo_text_out: TEXT_SURFACE_BLENDED
			-- Text du `combo_meter'

	combo_texture_in, combo_texture_out: GAME_TEXTURE
			-- Texture du `combo_meter'

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "[C�gep de Drummondville]"
end
