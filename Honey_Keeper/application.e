note
	description: "Classe principale qui d�marre l'application."
	author: "Andronik Karkaselis"
	date: "27-05-2020"

class
	APPLICATION

inherit

	GAME_LIBRARY_SHARED
	TEXT_LIBRARY_SHARED
	IMG_LIBRARY_SHARED
	AUDIO_LIBRARY_SHARED

create
	make

feature {NONE} -- Initialisation

	make
			-- Boucle principale du jeu qui ouvre les librairies.
		do
			game_library.enable_video
			text_library.enable_text
			audio_library.enable_playback
			image_file_library.enable_image (True, False, False)
			run_main_menu
			game_library.clear_all_events
			text_library.quit_library
			game_library.quit_library
		end

	run_main_menu
			-- Lance le menu principal.			
		local
			l_main_menu: MENU_MAIN
		do
			create l_main_menu.make
			game_library.enable_print_on_error
			has_error := False
			l_main_menu.run
		end

	has_error:BOOLEAN
			-- Contient un erreur

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"

end
