note
	description:  "Classe qui g�re la m�chanique du jeux."
	author: "Andronik Karkaselis"
	date: "27-05-2020"

class
	GAME_ENGINE

inherit
	ENGINE
		redefine
			update_scene,
			on_mouse_up
		end

	GAME_RANDOM_SHARED

create
	make_game

feature {NONE} -- Initialisation

	make_game (a_window: WINDOW; a_music: AUDIO; a_click: AUDIO; a_background: BACKGROUND)
				-- Construit le jeu principal avec les tuiles fissur�es et leurs coordonn�es dans les listes.
		do
			create background.make (a_window)
			make(a_window, a_music, a_click, background)
			create {LINKED_LIST [CRACKED_TILE]} tile_list.make
			across 0 |..| 2 as la_y loop
				across 1 |..| 3 as la_x loop
					tile_list.extend (create {CRACKED_TILE}.make_crack (a_window, la_x.item*200, la_y.item*200))
				end
			end
			active_tile := tile_list.first
			create player_score.make (a_window.window.renderer)
			create high_score.make (a_window.window.renderer)

			create chime.make ("./Sons/chime_simple.wav")
			create chime_combo.make ("./Sons/chime_combo.wav")
			create chime_bonus.make ("./Sons/chime_bonus.wav")
			if click.is_muted then
				chime.mute
				chime_combo.mute
				chime_bonus.mute
			end

			create timer.make
			difficulty := 0
			background.set_moving (False)

			create net.make
			get_highscore

		end

feature {ANY} -- Acc�s

	run_game (a_timestamp: NATURAL_32)
			-- D�marre les m�chaniques du jeu.
		require
			No_Error: not has_error
		do
			game_library.clear_all_events
			window.window.renderer.clear
			game_library.quit_signal_actions.extend (agent on_quit)
			window.window.mouse_button_pressed_actions.extend (agent on_mouse_down)
			window.window.mouse_button_released_actions.extend (agent on_mouse_up)
			window.window.mouse_motion_actions.extend (agent on_mouse_move)
			game_library.iteration_actions.extend (agent update_scene)
			timer.reset_timer (a_timestamp)
			active_tile := random_tile_selection
			update_scene (a_timestamp)
			game_library.launch
		end

	get_highscore
			-- Recoit le nom et le score record et les envoie dans la classe `highscore'
		local
			l_name, l_score: STRING_8
		do
			l_name := " "
			l_score := " "
			net.client ("highscore_request")
			net.client_listener
			if not net.highscore_list.is_empty then
				l_name := net.highscore_list[1]
				l_score := net.highscore_list[2]
			end
			high_score.set_highscore (l_score, l_name)
--			io.put_string ("%Ninside get_highscore "+l_name + l_score + "%N")
		end

	main_game_mechanic (a_timestamp: NATURAL_32)
			-- M�thode qui g�re l'affichage des tuiles avec une fissure.
		do
			if not active_tile.is_gameover then
				active_tile.update (timer.delta_time(a_timestamp))
			else
				if timer.delta_time (a_timestamp) > 2000 then
					launch_gameover_menu
				end
			end
			active_tile.draw
			if attached previous_active_tile as la_tile and timer.delta_time (a_timestamp) < 100 then
				la_tile.draw
			end
		end

	update_scene (a_timestamp: NATURAL_32)
			-- Redessine les �l�ments de la fen�tre et affecte les m�chaniques du jeu.
		do
			precursor (0)
			player_score.display_score
			player_score.display_combo
			high_score.display_highscore
			main_game_mechanic (a_timestamp)
			window.window.renderer.present
		end

	random_tile_selection: CRACKED_TILE
			-- S�lectionne al�atoirement une tuile de 1 � 9 et l'assigne � `active_tile'
		local
			l_tile: CRACKED_TILE
		do
			random.generate_new_random
			l_tile := tile_list.at (random.last_random_integer_between (1, tile_list.count))
			if l_tile = previous_active_tile then
				l_tile := random_tile_selection
			end
			Result := l_tile
		end

	on_mouse_up(a_timestamp: NATURAL_32; a_mouse_event: GAME_MOUSE_BUTTON_RELEASE_EVENT; a_nb_clicks: NATURAL_8)
			-- M�thode qui g�re le relachement d'un clic dans la fen�tre et lance `validate_tile'.
		do
			precursor (a_timestamp, a_mouse_event, a_nb_clicks)
			if not active_tile.is_gameover then
				validate_tile (a_timestamp)
			end
		end

	validate_tile (a_timestamp: NATURAL_32)
			-- V�rifie si le clic se trouve aux m�mes coordonn�es qu'une tuile active (`active_tile').
		do
			if active_tile.hitbox.is_inside(last_x, last_y) then
				if	active_tile.hitbox_bonus.is_inside(last_x, last_y) then
					chime_combo.play_once
					player_score.update_score (25)
					player_score.update_combo
					if player_score.is_bonus then
						chime_bonus.play_once
						player_score.update_score (75)
					end
				else
					chime.play_once
					player_score.reset_combo_meter
					player_score.update_score (10)
				end
				increase_difficulty
				prepare_next_tile (a_timestamp)
			else
				player_score.reset_combo_meter
			end
		end

	increase_difficulty
			-- Augmente la difficult� de 2 millisecondes
		do
			difficulty := difficulty + 2
			active_tile.set_difficulty (difficulty)
		end

	prepare_next_tile (a_timestamp: NATURAL_32)
			-- Pr�pare la prochaine tuile fissur�e
		do
			active_tile.check_bonus (player_score.is_bonus)
			active_tile.fix
			timer.reset_timer (a_timestamp)
			previous_active_tile := active_tile
			active_tile := random_tile_selection
		end

feature {NONE}

	launch_gameover_menu
			-- Lance le menu de fin de jeu
		local
			l_gameover_menu: MENU_GAMEOVER
		do
			game_library.clear_all_events
			create l_gameover_menu.make (window, music, click, background)
			l_gameover_menu.set_final_score (player_score.formatted_score, high_score.highscore)
			l_gameover_menu.check_highscore
			l_gameover_menu.run
			l_gameover_menu.send_highscore
			quit := l_gameover_menu.quit
			return_previous_menu := l_gameover_menu.return_previous_menu
			create net.make
			get_highscore
		end

feature {ANY} -- Implementation

	active_tile: CRACKED_TILE
			-- Tuile active avec des fissures

	previous_active_tile: detachable CRACKED_TILE
			-- Tuile qui �tait pr�c�demment active.

	player_score: SCORE
			-- Le score du joueur

	high_score: HIGHSCORE
			-- Le score record

	difficulty: NATURAL_32
			-- Le temps en millisecondes qui est enlev� pour clicker sur la tuile fissur�e

	timer: TIMER
			-- la classe qui g�re le temps

	tile_list:LIST[CRACKED_TILE]
			-- Liste des positions des tuiles du jeu

	chime, chime_combo: AUDIO
			-- Effet sonore pour indiquer un bon click

	chime_bonus: AUDIO
			-- Effet sonore qui indique que le joueur � eu le bonus

	net: NETWORK
			-- Classe qui g�re les fonctionnalit�s r�seau


invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"

end
