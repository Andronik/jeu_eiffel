note
	description:  "Classe qui g�re le thread pour la connection r�seau."
	author: "Andronik Karkaselis"
	date: "27-05-2020"

class
	NET_THREAD

inherit

	THREAD
		rename
			make as make_thread
		end

create
	make

feature {NONE}

	make (a_socket: NETWORK_DATAGRAM_SOCKET)
			-- Cr�e le `highscore_list' et assigne `a_socket' � `socket'
		do
			make_thread
			is_over := False
			socket := a_socket
			information := ""
			create {LINKED_LIST [STRING]} highscore_list.make
		ensure
			Assigned_socket: socket = a_socket
		end

feature {ANY} -- Acces

	execute
			-- Recoit l'information du serveur
		local
			l_info_size: INTEGER
			l_retry: BOOLEAN
		do
			if not l_retry then
				socket.read_integer
				l_info_size := socket.last_integer
				socket.read_stream (l_info_size)
				information := socket.last_string
				if not information.is_empty then
					highscore_list.extend (information)
				end

				socket.read_integer
				l_info_size := socket.last_integer
				socket.read_stream (l_info_size)
				information := socket.last_string
				if not information.is_empty then
					highscore_list.extend (information)
				end
			end
			end_thread
		rescue
			l_retry := True
			retry
		end

	end_thread
			-- Determine la fin du thread
		do
			is_over := True
		end


feature {ANY} -- Implementation

	socket: NETWORK_DATAGRAM_SOCKET
			-- Le socket utilis� pour communiquer

	information: STRING
			-- L'information envoy�e au serveur

	highscore_list: LIST [STRING]
			-- Liste contenant le nom et le score record.

	is_over: BOOLEAN
			-- Indique si le thread est termin�

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"

end
