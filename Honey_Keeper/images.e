note
	description: "Classe qui contient les m�thodes pour la cr�ation d'une image."
	author: "Andronik Karkaselis"
	date: "27-05-2020"

deferred class
	IMAGES

inherit
	GAME_TEXTURE

feature {ANY} -- Initialisation

	make_image (a_renderer:GAME_RENDERER; a_file_name:STRING)
			-- M�thode qui cr�e une image � partir d'un fichier `a_file_name' dans la fen�tre `a_renderer'
		local
			l_image: IMG_IMAGE_FILE
		do
			create l_image.make (a_file_name)
			if l_image.is_openable then
				l_image.open
				if l_image.is_open then
					make_from_image (a_renderer, l_image)
				else
					has_error := True
				end
			else
				has_error := True
			end
		end

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"
end
