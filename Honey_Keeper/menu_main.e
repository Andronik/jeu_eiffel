note
	description: "Classe qui g�re le menu principal avec un boutton pour d�marrer le jeu et un pour quitter."
	author: "Andronik Karkaselis"
	date: "27-05-2020"

class
	MENU_MAIN

inherit
	MENU
		rename
			make as make_menu
		redefine
			update_scene,
			on_mouse_up,
			run
		end

create
	make

feature {NONE} -- Initialisation

	make
			-- Cr�e le menu principal dans la fen�tre
		require
			No_Error: not has_error
		local
			l_window: WINDOW
			l_background: BACKGROUND
			l_music: AUDIO
			l_click: AUDIO
		do
			create l_window.make
			create l_background.make (l_window)
			create l_music.make ("./Sons/lone-wolf.wav")
			create l_click.make ("./Sons/Click.wav")
			make_menu (l_window, l_music, l_click, l_background)
			create titre.make_displayable (window.window.renderer, "./Images/titre.png")
			create play_button.make_displayable (window.window.renderer, "./Images/play_button.png")
			create options_button.make_displayable (window.window.renderer, "./Images/options_button.png")
			create aide_button.make_displayable (window.window.renderer, "./Images/aide_button.png")
			create quit_button.make_displayable (window.window.renderer, "./Images/quit_button.png")
			coordinates_list.extend (play_button_coordinates)
			coordinates_list.extend (options_button_coordinates)
			coordinates_list.extend (aide_button_coordinates)
			coordinates_list.extend (quit_button_coordinates)
		end

feature {ANY} -- Acc�s

	run
			-- D�marre la musique et la joue jusqu'� la fermeture du jeu.
		do
			music.play_loop
			from
				quit := False
			until
				quit
			loop
				precursor {MENU}
			end
		end

	update_scene (a_timestamp: NATURAL_32)
			-- M�thode qui redessine l'affichage et lance les bouttons du menu
		do
			precursor (a_timestamp)
			launch_main_menu_buttons
			window.window.renderer.present
			window.window.update
		end

	on_mouse_up(a_timestamp: NATURAL_32; a_mouse_event: GAME_MOUSE_BUTTON_RELEASE_EVENT; a_nb_clicks: NATURAL_8)
			-- M�thode qui g�re le relachement d'un clic dans la fen�tre et valide les boutons
		do
			precursor (a_timestamp, a_mouse_event, a_nb_clicks)
			validate_button (a_timestamp)
		end

	validate_button (a_timestamp: NATURAL_32)
			-- V�rifie si le clic se trouve dans les coordonn�es du bouton 'play' (`play_button_coordinates').
		do
			if last_x > 400 and last_x < 600 then
				if last_y > play_button_coordinates.y1 and last_y < play_button_coordinates.y2 then
					click.play_once
					launch_main_game (a_timestamp)
				elseif last_y > options_button_coordinates.y1 and last_y < options_button_coordinates.y2 then
					click.play_once
					launch_options_menu
				elseif last_y > aide_button_coordinates.y1 and last_y < aide_button_coordinates.y2 then
					click.play_once
					launch_aide_menu
				elseif last_y > quit_button_coordinates.y1 and last_y < quit_button_coordinates.y2 then
					on_quit (0)
				end
			end
		end

feature {NONE}

	launch_main_menu_buttons
			-- M�thode qui g�re l'affichage des �l�ments du menu principal.
		do
			titre.display (300, 40, window.window.renderer)
			play_button.display (play_button_coordinates.x1, play_button_coordinates.y1, window.window.renderer)
			options_button.display (options_button_coordinates.x1, options_button_coordinates.y1, window.window.renderer)
			aide_button.display (aide_button_coordinates.x1, aide_button_coordinates.y1, window.window.renderer)
			quit_button.display (quit_button_coordinates.x1, quit_button_coordinates.y1, window.window.renderer)
		end

	launch_main_game (a_timestamp: NATURAL_32)
			-- D�marre le jeu.
		local
			l_main_game: GAME_ENGINE
		do
			create l_main_game.make_game (window, music, click, background)
			l_main_game.run_game (a_timestamp)
			quit := l_main_game.quit
		end

	launch_options_menu
			-- Lance le menu 'options'
		local
			l_options_menu: MENU_OPTIONS
		do
			create l_options_menu.make (window, music, click, background)
			l_options_menu.run
			quit := l_options_menu.quit
			return_previous_menu := l_options_menu.return_previous_menu
		end

	launch_aide_menu
			-- Lance le menu 'aide'
		local
			l_aide_menu: MENU_AIDE
		do
			create l_aide_menu.make (window, music, click, background)
			l_aide_menu.run
			quit := l_aide_menu.quit
			return_previous_menu := l_aide_menu.return_previous_menu
		end

feature

	titre: DISPLAYABLE
			-- L'image du titre

	play_button: DISPLAYABLE
			-- L'image du bouton jouer

	options_button: DISPLAYABLE
			-- L'image du bouton

	aide_button: DISPLAYABLE
			-- L'image du bouton

	quit_button: DISPLAYABLE
			-- L'image du bouton quitter

	play_button_coordinates: TUPLE [x1, y1, x2, y2:INTEGER]
			-- Liste de coordonn�es de `play_button'
		once
			Result := [400, 180, 600, 235]
		end

	options_button_coordinates: TUPLE [x1, y1, x2, y2:INTEGER]
			-- Liste de coordonn�es de `options_button'
		once
			Result := [400, 280, 600, 335]
		end

	aide_button_coordinates: TUPLE [x1, y1, x2, y2:INTEGER]
			-- Liste de coordonn�es de `aide_button'
		once
			Result := [400, 380, 600, 435]
		end

	quit_button_coordinates: TUPLE [x1, y1, x2, y2:INTEGER]
			-- Liste de coordonn�es de `quit_button'
		once
			Result := [400, 480, 600, 535]
		end

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"
end
