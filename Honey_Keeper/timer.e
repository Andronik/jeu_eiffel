note
	description: "Classe qui g�re le minuteur du jeu principal."
	author: "Andronik Karkaselis"
	date: "27-05-2020"

class
	TIMER

create
	make

feature {NONE} -- Initialisation

	make
			-- Initialisation du comteur de temps
		do
			old_timestamp := 0
		end

feature {ANY} -- Impl�mentation

	delta_time (a_timestamp: NATURAL_32): NATURAL_32
			-- M�thode qui g�re la diff�rence de temps
		do
			Result := a_timestamp - old_timestamp
		end

	reset_timer (a_timestamp: NATURAL_32)
			-- M�thode qui red�marre le temps de l'�volution d'une fissure
		do
			old_timestamp := a_timestamp
		end

	old_timestamp: NATURAL_32
			-- Rep�re de temps pour calculer la diff�rence avec le temps �coul�

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "[C�gep de Drummondville]"
end
