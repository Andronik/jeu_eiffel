note
	description: "Tests pour la classe {MENU_GAMEOVER}"
	author: "EiffelStudio test wizard"
	date: "27-05-2020"
	revision: "0.1"
	testing: "type/manual"

class
	TEST_MENU_GAMEOVER

inherit
	EQA_TEST_SET
		redefine
			on_prepare,
			on_clean
		end

	GAME_LIBRARY_SHARED
		undefine
			default_create
		end
	IMG_LIBRARY_SHARED
		undefine
			default_create
		end
	TEXT_LIBRARY_SHARED
		undefine
			default_create
		end
	AUDIO_LIBRARY_SHARED
		undefine
			default_create
		end
	MENU_GAMEOVER
		undefine
			default_create
		end

feature {NONE} -- Events

	on_prepare
			-- Pr�pare MENU_GAMEOVER
		do
			game_library.enable_video
			text_library.enable_text
			audio_library.enable_playback
			image_file_library.enable_image (True, False, False)
			create window.make
			create background.make (window)
			create music.make ("./Sons/lone-wolf.wav")
			create click.make ("./Sons/Click.wav")
			make (window, music, click, background)
		end

	on_clean
			-- D�truit MENU_GAMEOVER
		do
			game_library.clear_all_events
			image_file_library.quit_library
			game_library.quit_library
		end

feature -- Test routines

	test_set_final_score_normal
			-- Test set_final_score normal
		note
			testing:  "covers/{MENU_GAMEOVER}.set_final_score"
		do
			set_final_score ("the_score", "")
			assert ("set_final_score test normal", final_score ~ "the_score")
		end

	test_set_final_score_limite
			-- Test set_final_score limite
		note
			testing:  "covers/{MENU_GAMEOVER}.set_final_score"
		do
			set_final_score ("", "")
			assert ("set_final_score test normal", final_score ~ "")
		end

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"
end


