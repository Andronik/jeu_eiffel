note
	description: "Tests pour la classe {HIGHSCORE}"
	author: "EiffelStudio test wizard"
	date: "27-05-2020"
	revision: "0.1"
	testing: "type/manual"

class
	TEST_HIGHSCORE

inherit
	EQA_TEST_SET
		redefine
			on_prepare,
			on_clean
		end

	GAME_LIBRARY_SHARED
		undefine
			default_create
		end
	IMG_LIBRARY_SHARED
		undefine
			default_create
		end
	TEXT_LIBRARY_SHARED
		undefine
			default_create
		end
	AUDIO_LIBRARY_SHARED
		undefine
			default_create
		end
	HIGHSCORE
		undefine
			default_create
		end

feature {NONE} -- Events

	on_prepare
			-- Pr�pare HIGHSCORE
		do
			game_library.enable_video
			text_library.enable_text
			audio_library.enable_playback
			image_file_library.enable_image (True, False, False)
			create renderer_window.make
			make (renderer_window.window.renderer)
		end

	on_clean
			-- D�truit HIGHSCORE
		do
			game_library.clear_all_events
			image_file_library.quit_library
			game_library.quit_library
		end

feature -- Test routines

	test_set_highscore_normal
			-- New test routine
		note
			testing:  "covers/{HIGHSCORE}", "execution/isolated", "execution/serial"
		do
			set_highscore ("score", "name")
			assert ("set_highscore test normal", highscore.is_equal ("score"))
			assert ("set_highscore test normal", highscore_name.is_equal ("name"))
		end

feature

	renderer_window: WINDOW
			-- Fen�tre de jeu

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "[C�gep de Drummondville]"
end


