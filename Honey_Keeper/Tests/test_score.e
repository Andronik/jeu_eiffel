note
	description: "Tests pour la classe {SCORE}"
	author: "EiffelStudio test wizard"
	date: "27-05-2020"
	revision: "0.1"
	testing: "type/manual"

class
	TEST_SCORE

inherit
	EQA_TEST_SET
		redefine
			on_prepare,
			on_clean
		end

	GAME_LIBRARY_SHARED
		undefine
			default_create
		end
	IMG_LIBRARY_SHARED
		undefine
			default_create
		end
	TEXT_LIBRARY_SHARED
		undefine
			default_create
		end
	AUDIO_LIBRARY_SHARED
		undefine
			default_create
		end
	SCORE
		undefine
			default_create
		end

feature {NONE} -- Events

	on_prepare
			-- Pr�pare SCORE
		do
			game_library.enable_video
			text_library.enable_text
			audio_library.enable_playback
			image_file_library.enable_image (True, False, False)
			create renderer_window.make
			make (renderer_window.window.renderer)
		end

	on_clean
			-- D�truit SCORE
		do
			game_library.clear_all_events
			image_file_library.quit_library
			game_library.quit_library
		end

feature -- Test routines

	test_update_score_normal
			-- Test pour `update_score' normal
		note
			testing:  "covers/{SCORE}", "execution/isolated", "execution/serial"
		do
			current_score := 0
			update_score (36)
			assert ("update_score test normal", formatted_score.is_equal ("000036"))
		end

	test_update_score_limite
			-- Test pour `update_score' limite
		note
			testing:  "covers/{SCORE}", "execution/isolated", "execution/serial"
		do
			current_score := 0
			update_score (0)
			assert ("update_score test limite", formatted_score.is_equal ("000000"))
		end

feature

	renderer_window: WINDOW
			-- Fen�tre de jeu

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "[C�gep de Drummondville]"
end


