note
	description: "Tests pour la classe {HIGHSCORE}"
	author: "EiffelStudio test wizard"
	date: "27-05-2020"
	revision: "0.1"
	testing: "type/manual"

class
	TEST_HITBOX

inherit
	EQA_TEST_SET

	HITBOX
		undefine
			default_create
		end

feature -- Test routines

	test_make_normal
			-- test du make normal
		note
			testing:  "covers/{HITBOX}.make", "execution/isolated", "execution/serial"
		do
			make (0, 0, 10, 10)
			assert ("make test normal", x1 = 0)
			assert ("make test normal", y1 = 0)
			assert ("make test normal", x2 = 10)
			assert ("make test normal", y2 = 10)
		end

	test_is_inside_normal
			-- Test `is_inside'
		note
			testing:  "covers/{HITBOX}.is_inside", "execution/isolated", "execution/serial"
		do
			make (0, 0, 10, 10)
			assert ("make is_inside test normal", is_inside (5, 5) = True)
		end

	test_is_inside_limite
			-- Test `is_inside'
		note
			testing:  "covers/{HITBOX}.is_inside", "execution/isolated", "execution/serial"
		do
			make (0, 0, 10, 10)
			assert ("make is_inside test normal", is_inside (1, 1) = True)
		end

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "[C�gep de Drummondville]"
end

