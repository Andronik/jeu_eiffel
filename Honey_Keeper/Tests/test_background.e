note
	description: "Tests pour la classe {BACKGROUND}"
	author: "EiffelStudio test wizard"
	date: "27-05-2020"
	revision: "0.1"
	testing: "type/manual"

class
	TEST_BACKGROUND

inherit
	EQA_TEST_SET
		redefine
			on_prepare,
			on_clean
		end

	GAME_LIBRARY_SHARED
		undefine
			default_create
		end
	IMG_LIBRARY_SHARED
		undefine
			default_create
		end
	TEXT_LIBRARY_SHARED
		undefine
			default_create
		end
	AUDIO_LIBRARY_SHARED
		undefine
			default_create
		end
	BACKGROUND
		undefine
			default_create
		end

feature {NONE} -- Events

	on_prepare
			-- Pr�pare le background
		do
			game_library.enable_video
			text_library.enable_text
			audio_library.enable_playback
			image_file_library.enable_image (True, False, False)
			create window.make
			make (window)
		end

	on_clean
			-- D�truit le background
		do
			game_library.clear_all_events
			image_file_library.quit_library
			game_library.quit_library
		end

feature -- Test routines

	test_moving_gauche_normal
			-- Test que le mouvement � gauche se fait correctement
		note
			testing:  "covers/{BACKGROUND}", "execution/isolated"
		do
			tile_x := 0
			is_moving := True
			moving_background (10)
			assert ("moving_background test normal", tile_x = 1)
		end

	test_moving_gauche_limite
			-- Test que le mouvement � gauche se fait correctement
		note
			testing:  "covers/{BACKGROUND}", "execution/isolated"
		do
			tile_x := 0
			is_moving := True
			moving_background (0)
			assert ("moving_background test limite", tile_x = 1)
		end

	test_moving_droite_normal
			-- Test que le mouvement � droite se fait correctement
		note
			testing:  "covers/{BACKGROUND}", "execution/isolated"
		do
			tile_x := 0
			is_moving := True
			moving_background (600)
			assert ("moving_background test normal", tile_x = -1)
		end

	test_moving_droite_limite
			-- Test que le mouvement � gauche se fait correctement
		note
			testing:  "covers/{BACKGROUND}", "execution/isolated"
		do
			tile_x := 0
			is_moving := True
			moving_background (501)
			assert ("moving_background test limite", tile_x = -1)
		end

	test_not_moving_normal
			-- Test le mouvement de transition quand `is_moving' est � false
		note
			testing:  "covers/{BACKGROUND}", "execution/isolated"
		do
			tile_x := 0
			is_moving := False
			moving_background (0)
			assert ("moving_background test normal", tile_x = 10)
		end

	test_set_moving_normal
			-- Test que le `is_moving'
		note
			testing:  "covers/{BACKGROUND}", "execution/isolated"
		do
			is_moving := False
			set_moving(True)
			assert ("set_moving test normal", is_moving = True)
		end

feature

	window: WINDOW
			-- Fen�tre de jeu


invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "[C�gep de Drummondville]"
end
