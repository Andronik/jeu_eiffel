note
	description: "Tests pour la classe {CRACKED_TILE}"
	author: "EiffelStudio test wizard"
	date: "27-05-2020"
	revision: "0.1"
	testing: "type/manual"

class
	TEST_CRACKED_TILE

inherit
	EQA_TEST_SET
		redefine
			on_prepare,
			on_clean
		end
	GAME_LIBRARY_SHARED
		undefine
			default_create
		end
	IMG_LIBRARY_SHARED
		undefine
			default_create
		end
	TEXT_LIBRARY_SHARED
		undefine
			default_create
		end
	AUDIO_LIBRARY_SHARED
		undefine
			default_create
		end
	CRACKED_TILE
		undefine
			default_create
		end

feature {NONE} -- Events

	on_prepare
			-- Pr�pare cracked_tile
		do
			game_library.enable_video
			text_library.enable_text
			audio_library.enable_playback
			image_file_library.enable_image (True, False, False)
			create window.make
			make_crack (window, 200, 200)
		end

	on_clean
			-- D�truit cracked_tile
		do
			game_library.clear_all_events
			image_file_library.quit_library
			game_library.quit_library
		end

feature -- Test routines

	test_fix_normal
			-- Test le `fix'
		note
			testing:  "execution/isolated", "covers/{CRACKED_TILE}", "execution/serial"
		do
			is_fixed := False
			fix
			assert ("fix test normal", is_fixed = True)
		end

	test_update_normal
			-- Test le `update'
		note
			testing:  "execution/isolated", "covers/{CRACKED_TILE}", "execution/serial"
		do
			difficulty := 0
			prepare_cracked_change
			update (250)
			assert ("update test normal", is_cracked_1 = True)
			update (600)
			assert ("update test normal", is_cracked_2 = True)
			update (1300)
			assert ("update test normal", is_cracked_3 = True)
			assert ("update test normal", is_gameover = True)
		end

	test_update_limite
			-- Test le `update'
		note
			testing:  "execution/isolated", "covers/{CRACKED_TILE}", "execution/serial"
		do
			difficulty := 0
			prepare_cracked_change
			update (249)
			assert ("update test normal", is_cracked_1 = False)
			update (599)
			assert ("update test normal", is_cracked_1 = True)
			assert ("update test normal", is_cracked_2 = False)
			update (1299)
			assert ("update test normal", is_cracked_2 = True)
			assert ("update test normal", is_cracked_3 = False)
			assert ("update test normal", is_gameover = False)
			update (1300)
			assert ("update test normal", is_cracked_3 = True)
			assert ("update test normal", is_gameover = True)
		end

	test_check_bonus_normal
			-- Test le `check_bonus'
		note
			testing:  "execution/isolated", "covers/{CRACKED_TILE}", "execution/serial"
		do
			is_bonus := False
			check_bonus (True)
			assert ("check_bonus test normal", is_bonus = True)
		end

	test_set_difficulty_normal
			-- Test la m�thode `set_difficulty'
		note
			testing:  "execution/isolated", "covers/{CRACKED_TILE}", "execution/serial"
		do
			difficulty := 0
			set_difficulty (10)
			assert ("set_difficulty test normal", difficulty = 10)
		end

feature

	window: WINDOW
			-- Fen�tre de jeu

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "[C�gep de Drummondville]"
end

