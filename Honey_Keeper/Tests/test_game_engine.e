note
	description: "Tests pour la classe {GAME_ENGINE}"
	author: "EiffelStudio test wizard"
	date: "27-05-2020"
	revision: "0.1"
	testing: "type/manual"

class
	TEST_GAME_ENGINE

inherit
	EQA_TEST_SET
		redefine
			on_prepare,
			on_clean
		end

	GAME_LIBRARY_SHARED
		undefine
			default_create
		end
	IMG_LIBRARY_SHARED
		undefine
			default_create
		end
	TEXT_LIBRARY_SHARED
		undefine
			default_create
		end
	AUDIO_LIBRARY_SHARED
		undefine
			default_create
		end
	GAME_ENGINE
		undefine
			default_create
		end

feature {NONE} -- Events

	on_prepare
			-- Pr�pare ENGINE
		do
			game_library.enable_video
			text_library.enable_text
			audio_library.enable_playback
			image_file_library.enable_image (True, False, False)
			create window.make
			create background.make (window)
			create music.make ("./Sons/lone-wolf.wav")
			create click.make ("./Sons/Click.wav")
			make_game (window, music, click, background)
		end

	on_clean
			-- D�truit ENGINE
		do
			game_library.clear_all_events
			image_file_library.quit_library
			game_library.quit_library
		end

feature -- Test routines

	test_make_normal
			-- Test le make de GAME_ENGINE
		note
			testing:  "covers/{ENGINE}.make", "execution/isolated", "execution/serial"
		do
--			assert ("make_game test normal", difficulty = 0)
		end


invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "[C�gep de Drummondville]"
end


