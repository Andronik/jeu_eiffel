note
	description: "Classe qui cr�e et affiche les tuiles crack�es."
	author: "Andronik Karkaselis"
	date: "27-05-2020"

class
	CRACKED_TILE

create
	make_crack

feature {NONE} -- Initialisation

	make_crack (a_window: WINDOW; a_x, a_y:INTEGER)
			-- Cr�e les tuiles fissur�es.
		do
			x := a_x
			y := a_y
			renderer := a_window.window.renderer
			create hitbox.make (x + 40, y + 50, x + 170, y + 150)
			create hitbox_bonus.make (hitbox.x1 + 50, hitbox.y1 + 30, hitbox.x2 - 50, hitbox.y2 - 30)
			create crack1.make_displayable (a_window.window.renderer, "./Images/tile_crack_1.png")
			create crack2.make_displayable (a_window.window.renderer, "./Images/tile_crack_2.png")
			create crack3.make_displayable (a_window.window.renderer, "./Images/tile_crack_3.png")
			create crack_fixed.make_displayable (a_window.window.renderer, "./Images/tile_fixed.png")
			create bonus_tile.make_displayable (a_window.window.renderer, "./Images/bonus_tile.png")
		end

feature {ANY} -- Acc�s

	is_cracked_1:BOOLEAN
			-- Indique si on est au premier stade de la fissure

	is_cracked_2:BOOLEAN
			-- Indique si on est au deuxi�me stade de la fissure

	is_cracked_3:BOOLEAN
			-- Indique si on est au troisi�me stade de la fissure

	is_bonus: BOOLEAN
			-- Indique si la tuile est une tuile bonus

	is_fixed:BOOLEAN
			-- Indique si la tuile est en r�paration

	is_gameover: BOOLEAN
			-- Indique si le jeu est termin�

	x:INTEGER
			-- Position sur l'axe 'x'

	y:INTEGER
			-- Position sur l'axe 'y'

	fix
			-- Pr�pare les images de fissure et met `is_fixed' � True
		do
			prepare_cracked_change
			is_fixed := True
		ensure
			is_fixed = True
		end

	update (a_delta_time: NATURAL_32)
			-- Actualise l'�volution de la fissure selon le temps `a_delta_time'
		local
			l_t2, l_t3: NATURAL_32
		do
			l_t2 := 600 - (difficulty//2)
			l_t3 := 1300 - difficulty
			prepare_cracked_change
			if a_delta_time >= 250 and a_delta_time < l_t2 then
				is_cracked_1 := True
			elseif a_delta_time >= l_t2 and a_delta_time < l_t3 then
				is_cracked_2 := True
			elseif a_delta_time >= l_t3 then
				is_cracked_3 := True
				is_gameover := True
			end
		end

	draw
			-- Affiche l'�volution de la fissure selon le temps dans une certaine tuile.		
		do
			if is_cracked_1 then
				crack1.display (x, y, renderer)
			elseif is_cracked_2 then
				crack2.display (x, y, renderer)
			elseif is_cracked_3 then
				crack3.display (x, y, renderer)
			elseif is_fixed then
				crack_fixed.display (x, y, renderer)
				if is_bonus then
					bonus_tile.display (x, y, renderer)
				end
			end
		end

	check_bonus (a_bonus_bool: BOOLEAN)
			-- V�rifie si le `combo_meter' est � la 5i�me �tape.
		do
			is_bonus := a_bonus_bool
		end

	set_difficulty (a_difficulty: NATURAL_32)
			-- Ajuste `difficulty' selon `a_difficulty'
		require
			Valid_number: a_difficulty >= 0
		do
			difficulty := a_difficulty
		end

	hitbox:HITBOX
			-- La boite de collision pour un click simple

	hitbox_bonus:HITBOX
			-- La boite de collision pour un click pr�cis

feature {NONE} -- Impl�mentation

	prepare_cracked_change
			-- Pr�pare les tuiles fissur�s
		do
			is_cracked_1 := False
			is_cracked_2 := False
			is_cracked_3 := False
			is_fixed := False
			is_gameover := False
		end

	renderer:GAME_RENDERER
			-- La fenetre o� afficher.

	difficulty: NATURAL_32
			-- La difficult� du jeu qui augmente avec le temps.

	crack1: DISPLAYABLE
			-- L'image de la premi�re phase de la fissure.

	crack2: DISPLAYABLE
			-- L'image de la deuxi�me phase de la fissure.

	crack3: DISPLAYABLE
			-- L'image de la troisi�me phase de la fissure.

	crack_fixed: DISPLAYABLE
			-- L'image de la tuile qui reg�n�re.

	bonus_tile: DISPLAYABLE
			-- L'image de la tuile bonus.

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "[C�gep de Drummondville]"
end
