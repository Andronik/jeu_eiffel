note
	description: "Classe qui g�n�re la fen�tre de l'application."
	author: "Andronik Karkaselis"
	date: "27-05-2020"

class
	WINDOW

inherit
	GAME_LIBRARY_SHARED
	IMG_LIBRARY_SHARED

create
	make

feature {NONE} -- Initialisation

	make
			-- Construit la fen�tre de l'application
		local
			l_window_builder: GAME_WINDOW_RENDERED_BUILDER
		do
			create l_window_builder
			l_window_builder.set_dimension (1000, 600)
			l_window_builder.set_title ("Honey Keeper")
			window := l_window_builder.generate_window
		end

feature {ANY} -- Impl�mentation

	window: GAME_WINDOW_RENDERED
			-- La fen�tre de l'application

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "[C�gep de Drummondville]"
end
