note
	description: "Classe g�re tout ce qui est reli� au curseur et son effect lors d'un click."
	author: "Andronik Karkaselis"
	date: "27-05-2020"

class
	PLAYER_CURSOR

inherit
	GAME_LIBRARY_SHARED

create
	make

feature {NONE} -- Initialisation

	make
			-- Cr�e les images du curseur et les mets dans une liste
		local
			l_curseur_image1:IMG_IMAGE_FILE
			l_curseur_image2:IMG_IMAGE_FILE
		do
			create {ARRAYED_LIST[GAME_CURSOR]}cursors.make (2)
			create l_curseur_image1.make ("./Images/custom1.png")
			if l_curseur_image1.is_openable then
				l_curseur_image1.open
				if l_curseur_image1.is_open then
					cursors.extend (create {GAME_CURSOR}.make_from_image (l_curseur_image1, 18, 0))
				end
			end
			create l_curseur_image2.make ("./Images/custom2.png")
			if l_curseur_image2.is_openable then
				l_curseur_image2.open
				if l_curseur_image2.is_open then
					cursors.extend (create {GAME_CURSOR}.make_from_image (l_curseur_image2, 18, 0))
				end
			end
			cursor_index := 0
			cursor_count := cursors.count
			change_cursor
		end

feature {ANY} -- Acc�s

	change_cursor
			-- Change le curseur {GAME_CURSOR}
		do
			cursor_index := (cursor_index \\ cursor_count) + 1
			game_library.set_cursor (cursors.at (cursor_index))		-- Change the cursor
		end

	cursor_index:INTEGER
			-- L'index du curseur dans `cursors'

	cursor_count:INTEGER
			-- Le nombre d'�l�ments dans `cursors'

	cursors:LIST[GAME_CURSOR]
			-- Liste contenant les curseurs

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"
end
