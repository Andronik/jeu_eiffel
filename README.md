	Honey Keeper
--------------------

Author: Andronikos Karkaselis

------------------------------

Description:

Projet de programmation orientée objet en deuxième année de DEC.

Le but du jeu est de cliquer sur les hexagones qui commencent à fissurer 
avant qu'ils ne brisent complètement. Chaque héxagone réparé donne au 
joueur 10 points. Si le clic s'est fait au milieu de la tuile, le joueur 
reçoit 25 points et il augmente son combo. Le combo augmente avec chaque 
clic précis au milieu et a cinq étapes; un combo réussi donne 100 points 
au moment du clic. 

--------------------------------------------------------------------------

Installation:

Windows

	1. Installer EiffelStudio
	
		i. Télécharger EiffelStudio à cette adresse:
			https://sourceforge.net/projects/eiffelstudio/files/EiffelStudio%2019.05/Build_103187/Eiffel_19.05_gpl_103187-win64.msi/download
		
		ii. Installer le logiciel.
	
	2. Installer Eiffel Game2
		
		i. Télécharger Eiffel Game 2 à cette adresse:
			https://github.com/tioui/Eiffel_Game2/tree/windows_build
		
		ii. Décompressez "Eiffel_Game2.zip" et copiez le répertoire 
			"game2" dans le sous-répertoire d'EiffelStudio contrib/library. 
			(Ce répertoire se trouve généralement dans 
			"C:\Program Files\Eiffel Software\EiffelStudio xx.yy GPL")
		
		iii. Copier les fichiers ".dll" du fichier "DLL64.zip" 
			 (ou du fichier "DLL32.zip" si vous utilisez une version 
			 32-bit de Windows) dans le répertoire du projet.
	
	3. Lancer le jeu à partir d'EiffelStudio.
	
Ubuntu
	
	1. Installer EiffelStudio
		
		i. Ouvrir un terminal et entrer:
			
		# sudo add-apt-repository ppa:eiffelstudio-team/ppa 
		# sudo apt-get update 
		# sudo apt-get install eiffelstudio
	
	2. Installer les librairies C
		
		i. Dans le terminal:
		
		# sudo apt-get install libgles2-mesa-dev
		# sudo apt-get install libsdl2-dev libsdl2-gfx-dev libsdl2-image-dev libsdl2-ttf-dev libopenal-dev libsndfile1-dev libmpg123-dev libepoxy-dev libgl1-mesa-dev libglu1-mesa-dev
	
	3. Télécharger Eiffel Game 2
		
		i. Dans le terminal:
		
		# git clone https://github.com/tioui/Eiffel_Game2.git
	
	4. Renommer le répertoire à game2.
	
	5. Ajouter le répertoire game2 dans le répertoire contrib/library de EiffelStudio. (Ce répertoire se trouve généralement dans “/usr/lib/EiffelStudio_xx.yy” ou dans “/usr/local/Eiffel_xx.yy”.
	
	5. Compiler la librairie
		
		i. Naviguer dans le terminal à l'interieure du répertoire de Eiffel_Game2:
		
		# ./compile_c_library.sh
	
	6. Lancer le jeu à partir d'EiffelStudio.

-----------------------------------------------------------------------------

License:
	Copyright (c) 2020 Andronikos Karkaselis
	
	L'autorisation est accordée, gracieusement, à toute personne acquérant une
    copie de ce programme et des fichiers de documentation associés
    (le "Programme"), de commercialiser le Programme sans restriction,
    notamment les droits d'utiliser, de copier, de modifier, de fusionner, de
    publier, de distribuer, de sous-licencier et / ou de vendre des copies de
    le Programme, ainsi que d'autoriser les personnes auxquelles le
    Programme est fourni à le faire, sous réserve des conditions suivantes:

    La déclaration de copyright ci-dessus et la présente autorisation doivent
    être incluses dans toutes copies ou parties substantielles du
    Programme.

    LE PROGRAMME EST FOURNI "TEL QUEL", SANS GARANTIE D'AUCUNE SORTE,
    EXPLICITE OU IMPLICITE, NOTAMMENT SANS GARANTIE DE QUALITÉ MARCHANDE,
    D’ADÉQUATION À UN USAGE PARTICULIER ET D'ABSENCE DE CONTREFAÇON. EN AUCUN
    CAS, LES AUTEURS OU TITULAIRES DU DROIT D'AUTEUR NE SERONT RESPONSABLES DE
    TOUT DOMMAGE, RÉCLAMATION OU AUTRE RESPONSABILITÉ, QUE CE SOIT DANS LE
    CADRE D'UN CONTRAT, D'UN DÉLIT OU AUTRE, EN PROVENANCE DE, CONSÉCUTIF À OU
    EN RELATION AVEC LA BIBLIOTHÈQUE OU SON UTILISATION, OU AVEC D'AUTRES
    ÉLÉMENTS DE LA BIBLIOTHÈQUE.
