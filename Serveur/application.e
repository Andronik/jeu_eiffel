note
	description: "Classe qui g�re le 'highscore' de Honey Keeper"
	author: "Andronik Karkaselis"
	date: "20-05-06"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialisation

	make
			-- Ex�cution du serveur qui recois des donn�es du jeu
		local
			l_socket: NETWORK_DATAGRAM_SOCKET
			l_port: INTEGER
			l_taille_message: INTEGER
			l_message: STRING
		do
			l_port := 3636
			create l_socket.make_bound (l_port)
			create data.make
			from
			until
				end_connection
			loop
				l_socket.read_integer
				l_taille_message := l_socket.last_integer
				l_socket.read_stream (l_taille_message)
				l_message := l_socket.last_string
				if l_message.is_equal ("highscore_request") then
					send_highscore
				else
					data.write_to_file (l_message)
				end
				io.put_string ("listening... %N")
			end
			l_socket.close
		end

	send_highscore
			-- Serveur qui envoie par UDP, � 'Honey_Keeper' le `highscore' enregistr�
		local
			l_socket: NETWORK_DATAGRAM_SOCKET
			l_port: INTEGER
		do
			l_port := 3036
			data.read_from_file
			create l_socket.make_targeted ("localhost", l_port)
			l_socket.put_integer (data.highscore_list[1].count)
			l_socket.put_string (data.highscore_list[1])
			l_socket.put_integer (data.highscore_list[2].count)
			l_socket.put_string (data.highscore_list[2])
			l_socket.close
			io.put_string ("highscore sent%N")
		end


feature {ANY} -- Implementation

	end_connection: BOOLEAN
			-- Indique quand la connection doit arr�ter

	data: DATABASE
			-- La classe qui permet de changer et d'aller chercher les donn�es

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"

end
