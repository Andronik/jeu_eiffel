note
	description: "Classe qui enregistre et lis le 'highscore' dans un fichier pour Honey Keeper"
	author: "Andronik Karkaselis"
	date: "20-05-06"

class
	DATABASE

create
	make

feature {NONE} -- Initialisation

	make
			-- Initialise les variables pour lire ou �crire dans le fichier
		do
			file_contents := ""
			file_name := "highscore.txt"
			create file.make_with_name (file_name)
			create {LINKED_LIST [STRING]} highscore_list.make
		end

feature {ANY}

	read_from_file
			-- ouvre le fichier texte `file' et met le contenu dans `highscore_list'
		local
			l_text: STRING_8
		do
			file.open_read
			from
			until
				file.exhausted
			loop
				file.read_line
                l_text := file.last_string
                if l_text.has ('|') then
                    highscore_list := l_text.split ('|')
                end
			end
			file.close
		end

	write_to_file (a_new_highscore: STRING)
			-- Ouvre le fichier texte et �cris le `highscore' en effacant le contenu
		do
			file.open_write
			file.put_string (a_new_highscore)
			file.close
		end

feature {ANY}

	highscore_list: LIST [STRING]
			-- Liste qui contient le nom et le score

	file: PLAIN_TEXT_FILE
			-- Le fichier qui contient les donn�es

	file_contents: STRING_8
			-- Le contenu du fichier

	file_name: STRING
			-- Le nom du fichier

invariant
note
	copyright: "Tous droits r�serv�s (c) 2020, Andronikos Karkaselis"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"

end
